<?php

namespace Core;

/**
 *
 */
interface IRepository
{
    /**
     * @return array
     */
    public function all(): array;

    /**
     * @param array $data
     * @return int
     */
    public function add(array $data): int;

    /**
     * @param int $i
     * @return array
     */
    public function get(int $i): array;

    /**
     * @param int $i
     * @return bool
     */
    public function delete(int $i): bool;
}