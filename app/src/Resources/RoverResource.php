<?php

namespace Resources;

use Exceptions\PlateauMissingException;
use Exceptions\RoverMissingException;
use Helpers\RoverHelper;
use Repositories\PlateauRepository;
use Repositories\RoverRepository;

/**
 *
 */
class RoverResource
{
    /**
     * @var RoverRepository
     */
    private $repository;
    /**
     * @var PlateauRepository
     */
    private $plateauRepository;

    /**
     * @var array
     */
    private $messages = [];

    /**
     *
     */
    public function __construct()
    {
        $this->repository = new RoverRepository();
        $this->plateauRepository = new PlateauRepository();
    }

    /**
     * @param int $plateauId
     * @param int $x
     * @param int $y
     * @param string $rotation
     * @return int
     * @throws PlateauMissingException
     */
    public function add(int $plateauId, int $x, int $y, string $rotation): int
    {

        $plateau = $this->plateauRepository->get($plateauId);

        if (empty($plateau)) {
            throw new PlateauMissingException('Plateau Bulunamadı');
        }

        $rovers = $this->repository->all();
        $rovers[$plateauId][] = ['x' => $x, 'y' => $y, 'rotate' => $rotation];
        $this->repository->add($rovers);
        return count($rovers[$plateauId]) - 1;
    }

    /**
     * @param int $pId
     * @param int $i
     * @return array
     */
    public function get(int $pId, int $i): array
    {
        return $this->repository->get($pId)[$i];
    }

    /**
     * @param int $pId
     * @param int $i
     * @return bool
     */
    public function delete(int $pId, int $i): bool
    {
        $rovers = $this->repository->all();
        unset($rovers[$pId][$i]);
        if (count($rovers[$pId]) == 0) {
            unset($rovers[$pId]);
        }

        $this->repository->add($rovers);
        return true;
    }

    /**
     * @param int $plateauId
     * @return array
     */
    public function list(int $plateauId): array
    {
        $result = [];
        $plateaus = $this->repository->all();
        foreach ($plateaus as $key => $plateau) {
            if ($key == $plateauId) {
                $result[] = [
                    'id' => $key,
                    'detail' => $plateau
                ];
            }
        }
        return $result;
    }

    /**
     * @param int $plateauId
     * @param int $roverId
     * @param string $commands
     * @return void
     * @throws RoverMissingException
     */
    public function moveRover(int $plateauId, int $roverId, string $commands)
    {
        $plateauDetail = $this->plateauRepository->get($plateauId);
        $plateau = $this->repository->get($plateauId);
        $rover = $plateau[$roverId];
        if (empty($rover)) {
            throw new RoverMissingException('Rover Bulunamadı.');
        }
        foreach (str_split($commands) as $command) {
            switch ($command) {
                case 'L':
                    $rover = $this->rotateToLeft($plateauId, $roverId, $rover);
                    break;
                case 'R':
                    $rover = $this->rotateToRight($plateauId, $roverId, $rover);
                    break;
                case 'M':
                    if (RoverHelper::canMove($plateauDetail, $rover)) {
                        $rover = $this->move($plateauId, $roverId, $rover);
                    } else {
                        $this->messages[] = [
                            'plateauId' => $plateauId,
                            'roverId' => $roverId,
                            'rover' => [
                                'x' => $rover['x'],
                                'y' => $rover['y'],
                                'rotate' => $rover['rotate']
                            ],
                            'plateau' => [
                                'x' => $plateauDetail['x'],
                                'y' => $plateauDetail['y']
                            ],
                            'message' => 'İleri gidemez.'
                        ];
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param array $rover
     * @param int $roverId
     * @param int $plateauId
     * @return array
     */
    private function move(int $plateauId, int $roverId, array $rover): array
    {
        $all = $this->repository->all();

        list($x, $y) = RoverHelper::move($rover);
        $rover['x'] = $x;
        $rover['y'] = $y;

        $all[$plateauId][$roverId] = $rover;
        $this->repository->add($all);
        return $rover;
    }

    /**
     * @param int $plateauId
     * @param int $roverId
     * @param array $rover
     * @return array
     */
    private function rotateToRight(int $plateauId, int $roverId, array $rover): array
    {
        $all = $this->repository->all();

        switch ($rover['rotate']) {
            case 'W':
                $rover['rotate'] = 'N';
                break;
            case 'N':
                $rover['rotate'] = 'E';
                break;
            case 'E':
                $rover['rotate'] = 'S';
                break;
            case 'S':
                $rover['rotate'] = 'W';
                break;
            default:
                break;
        }

        $all[$plateauId][$roverId] = $rover;
        $this->repository->add($all);
        return $rover;
    }

    /**
     * @param int $plateauId
     * @param int $roverId
     * @param array $rover
     * @return array
     */
    private function rotateToLeft(int $plateauId, int $roverId, array $rover): array
    {
        $all = $this->repository->all();

        switch ($rover['rotate']) {
            case 'W':
                $rover['rotate'] = 'S';
                break;
            case 'N':
                $rover['rotate'] = 'W';
                break;
            case 'E':
                $rover['rotate'] = 'N';
                break;
            case 'S':
                $rover['rotate'] = 'E';
                break;
            default:
                break;
        }

        $all[$plateauId][$roverId] = $rover;
        $this->repository->add($all);

        return $rover;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}