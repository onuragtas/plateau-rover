<?php

use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        define('PROJECT_DIR', __DIR__ . '/../');
    }
}