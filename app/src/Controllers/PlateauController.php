<?php

namespace Controllers;


use Core\Controller;
use Exceptions\ParameterException;
use Resources\PlateauResource;

/**
 *
 */
class PlateauController extends Controller
{

    /**
     * @return array
     * @throws ParameterException
     */
    public function addPlateau(): array
    {
        $parameters = $this->getJsonRequest();
        if (!isset($parameters['x']) || !isset($parameters['y'])) {
            throw new ParameterException('Eksik parametre');
        }

        $resource = new PlateauResource();
        $id = $resource->add($parameters['x'], $parameters['y']);
        return ['id' => $id];
    }

    /**
     * @return array
     */
    public function listPlateau(): array
    {
        $resource = new PlateauResource();
        return $resource->list();
    }

    /**
     * @param int $id
     * @return array
     */
    public function removePlateau(int $id): array
    {
        $resource = new PlateauResource();
        return [
            'deleted' => $resource->delete($id)
        ];
    }

}