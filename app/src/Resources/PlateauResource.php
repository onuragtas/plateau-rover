<?php

namespace Resources;

use Repositories\PlateauRepository;

/**
 *
 */
class PlateauResource
{

    /**
     * @var PlateauRepository
     */
    private $repository;

    /**
     *
     */
    public function __construct()
    {
        $this->repository = new PlateauRepository();
    }

    /**
     * @param int $x
     * @param int $y
     * @return int
     */
    public function add(int $x, int $y): int
    {
        return $this->repository->add(['x' => $x, 'y' => $y]);
    }

    /**
     * @param int $i
     * @return array
     */
    public function get(int $i): array
    {
        return $this->repository->get($i);
    }

    /**
     * @param int $i
     * @return bool
     */
    public function delete(int $i): bool
    {
        return $this->repository->delete($i);
    }

    /**
     * @return array
     */
    public function list(): array
    {
        $result = [];
        $plateaus = $this->repository->all();
        foreach ($plateaus as $key => $plateau) {
            $result[] = [
                'id' => $key,
                'detail' => $plateau
            ];
        }
        return $result;
    }


}