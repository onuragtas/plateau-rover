# plateau-rover

# Install
```
# composer install
```

# Install Docker
```
docker-compose up -d
```

# Add Plateau
```
curl --location --request POST 'http://localhost:8000/api/v1/plateau' \
--header 'Content-Type: application/json' \
--data-raw '{
    "x": 101,
    "y": 2001
}'
```

# List Plateaus
```
curl --location --request GET 'http://localhost:8000/api/v1/plateau'
```

# Delete Plateau
```
curl --location --request DELETE 'http://localhost:8000/api/v1/plateau/1'
```

# List Rover On Plateau Id
```
curl --location --request GET 'http://localhost:8000/api/v1/rover/[plateauId=0]'
```

# Add Rover To Plateu
```
curl --location --request POST 'http://localhost:8000/api/v1/rover' \
--header 'Content-Type: application/json' \
--data-raw '{
    "plateau_id": 0,
    "x": 101,
    "y": 2001,
    "rotate": "N"
}
```

# Send Command To Rover
```
curl --location --request PUT 'http://localhost:8000/api/v1/rover/[PlateauId=0]/[RoverId=0]' \
--header 'Content-Type: application/json' \
--data-raw '{
    "commands": "LMLMLMLMM"
}'
```

# Get Rover State
```
curl --location --request GET 'http://localhost:8000/api/v1/rover/[PlateauId=0]/[RoverId=0]'
```

# Delete Rover
```
curl --location --request DELETE 'http://localhost:8000/api/v1/rover/[PlateauId=0]/[RoverId=0]'
```

# Swagger Url
```
http://localhost:8000/web/index.html
```

# Tests
```
./vendor/bin/phpunit tests
```

# Tests In Docker
```
# docker exec -it mars_php bash
# ./vendor/phpunit/phpunit/phpunit tests
```

# Nginx Configuration
```
server {
    server_name rover.local.io;
    root /var/www/html/mars-rover1/public;
    index index.html index.php;

    location / {
        index index.php;
        # Check if a file or directory index file exists, else route it to
        try_files $uri /index.php?$query_string;
    }

    # set expiration of assets to MAX for caching
    location ~* \.(ico|css|js|gif)(\?[0-9]+)?$ {
        expires max;
        log_not_found off;
    }

    location ~* \.php$ {
        fastcgi_pass php72_xdebug:9000;
        include fastcgi.conf;
    }

    location ~ /files {
        deny all;
        return 404;
    }
}
```