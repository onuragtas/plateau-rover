<?php

namespace Core;

/**
 *
 */
abstract class Repository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @param array $object
     * @return int
     */
    protected function addItem(array $object): int
    {
        $data = $this->model->getData();
        $data[] = $object;
        $this->model->setData($data);
        return count($data) - 1;
    }

    /**
     * @param array $object
     * @return int
     */
    protected function setData(array $object): int
    {
        $this->model->setData($object);
        return count($object) - 1;
    }

    /**
     * @return array
     */
    protected function getAll(): array
    {
        return $this->model->getData();
    }

    /**
     * @param int $i
     * @return array
     */
    protected function getItem(int $i): array
    {
        if (isset($this->model->getData()[$i])) {
            return $this->model->getData()[$i];
        }

        return [];
    }

    /**
     * @param int $i
     * @return bool
     */
    protected function deleteItem(int $i): bool
    {
        if (isset($this->model->getData()[$i])) {
            $data = $this->model->getData();
            unset($data[$i]);
            $this->model->setData($data);
            return true;
        }

        return false;
    }
}