<?php

namespace Controllers;

use BaseTest;
use Exceptions\PlateauMissingException;
use Exceptions\RoverMissingException;
use Resources\PlateauResource;
use Resources\RoverResource;

/**
 *
 */
class RoverControllerTest extends BaseTest
{

    /**
     * @var RoverResource
     */
    private $resource;
    /**
     * @var PlateauResource
     */
    private $plateauResource;

    /**
     * @param $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->plateauResource = new PlateauResource();
        $this->resource = new RoverResource();
    }

    /**
     * @return void
     * @throws PlateauMissingException
     */
    public function testAdd()
    {
        $pId = $this->plateauResource->add(0, 0);
        $roverId = $this->resource->add($pId, 0, 0, 'N');
        $this->plateauResource->delete($pId);
        $this->resource->delete($pId, $roverId);
        $this->assertEquals(0, $roverId);
    }

    /**
     * @return void
     * @throws PlateauMissingException
     */
    public function testRemove()
    {
        $pId = $this->plateauResource->add(0, 0);
        $roverId = $this->resource->add($pId, 0, 0, 'N');
        $this->plateauResource->delete($pId);
        $result = $this->resource->delete($pId, $roverId);

        $this->assertIsBool(true, $result);
    }

    /**
     * @return void
     * @throws PlateauMissingException
     */
    public function testList()
    {
        $pId = $this->plateauResource->add(0, 0);
        $roverId = $this->resource->add($pId, 0, 0, 'N');

        $result = count($this->resource->list($pId));

        $this->plateauResource->delete($pId);
        $this->resource->delete($pId, $roverId);

        $this->assertNotEquals(0, $result);
    }

    /**
     * @return void
     * @throws PlateauMissingException
     */
    public function testGet()
    {
        $pId = $this->plateauResource->add(0, 0);
        $roverId = $this->resource->add($pId, 0, 0, 'N');

        $result = $this->resource->get($pId, $roverId);

        $this->plateauResource->delete($pId);
        $this->resource->delete($pId, $roverId);

        $this->assertNotEmpty($result);
    }

    /**
     * @return void
     * @throws PlateauMissingException
     * @throws RoverMissingException
     */
    public function testMove()
    {
        $pId = $this->plateauResource->add(10, 10);
        $roverId = $this->resource->add($pId, 0, 0, 'N');

        $this->resource->moveRover($pId, $roverId, 'MM');
        $result = $this->resource->get($pId, $roverId);


        $this->plateauResource->delete($pId);
        $this->resource->delete($pId, $roverId);

        $this->assertEquals(2, $result['y']);
    }
}
