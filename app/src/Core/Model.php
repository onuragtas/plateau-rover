<?php

namespace Core;

/**
 *
 */
abstract class Model
{

    /**
     * @var string
     */
    protected $file;
    /**
     * @var array
     */
    private $data;

    /**
     *
     */
    public function __construct()
    {
        $this->data = json_decode(file_get_contents(PROJECT_DIR . $this->file), true);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $this->data = json_decode(file_get_contents(PROJECT_DIR . $this->file), true);
        return $this->data;
    }

    /**
     * @param array $data
     * @return void
     */
    public function setData(array $data)
    {
        $this->data = $data;
        file_put_contents(PROJECT_DIR . $this->file, json_encode($data));
    }


}