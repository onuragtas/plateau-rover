<?php

namespace Models;

use Core\Model;

/**
 *
 */
class PlateauModel extends Model
{

    /**
     *
     */
    public function __construct()
    {
        $this->file = 'data/plateau.json';
        parent::__construct();
    }

}