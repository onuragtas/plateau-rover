<?php

use Controllers\PlateauController;
use Controllers\RoverController;
use Core\Router;

$roverRoute = '/api/v1/rover/([0-9]+)/([0-9]+)';

#Plateau
Router::getInstance()->addRoute('/api/v1/plateau/([0-9]+)', 'DELETE', PlateauController::class . ':removePlateau');
Router::getInstance()->addRoute('/api/v1/plateau', 'GET', PlateauController::class . ':listPlateau');
Router::getInstance()->addRoute('/api/v1/plateau', 'POST', PlateauController::class . ':addPlateau');

#Rover
Router::getInstance()->addRoute($roverRoute, 'PUT', RoverController::class . ':move');
Router::getInstance()->addRoute($roverRoute, 'GET', RoverController::class . ':get');
Router::getInstance()->addRoute('/api/v1/rover', 'POST', RoverController::class . ':add');
Router::getInstance()->addRoute('/api/v1/rover/([0-9]+)', 'GET', RoverController::class . ':list');
Router::getInstance()->addRoute($roverRoute, 'DELETE', RoverController::class . ':remove');
