<?php

namespace Core;

abstract class Controller
{

    /**
     * @return array
     */
    public function getJsonRequest(): array
    {
        return json_decode(file_get_contents('php://input'), true);
    }

}