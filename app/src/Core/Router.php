<?php

namespace Core;

use ArgumentCountError;
use Exceptions\RouteException;

/**
 *
 */
class Router
{
    /**
     * @var Router
     */
    private static $instance;

    /**
     * @var array
     */
    private $routes = [];

    /**
     * @return Router
     */
    public static function getInstance(): Router
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param string $route
     * @param string $method
     * @param string $object
     * @return void
     */
    public function addRoute(string $route, string $method, string $object)
    {
        $this->routes[$route][$method] = $object;
    }

    /**
     * @return array
     * @throws RouteException
     */
    public function runRoute(): array
    {
        list($uri, $method) = $this->getRequest();

        if (isset($this->routes[$uri]) && isset($this->routes[$uri][$method])) {
            try {
                return $this->run($uri, $method);
            } catch (ArgumentCountError $ex){
                // log
            }
        }

        foreach ($this->routes as $key => $route) {
            preg_match('|' . $key . '|si', $uri, $matches);
            if (count($matches) > 0 && isset($this->routes[$key]) && isset($this->routes[$key][$method])) {
                try {
                    return $this->run($key, $method, array_slice($matches, 1));
                }catch (ArgumentCountError $e) {
                    // log
                }
            }
        }

        throw new RouteException(RouteException::MISS_ROUTE);
    }

    /**
     * @return array
     */
    private function getRequest(): array
    {
        return [$_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']];
    }

    /**
     * @param string $uri
     * @param string $method
     * @param array $matches
     * @return array
     */
    private function run(string $uri, string $method, array $matches = []): array
    {
        list($object, $func) = explode(':', $this->routes[$uri][$method]);

        /** @var Controller $controller */
        $controller = new $object;
        return $controller->$func(...$matches);
    }

}