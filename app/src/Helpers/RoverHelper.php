<?php

namespace Helpers;

/**
 *
 */
class RoverHelper
{

    /**
     * @param array $plateau
     * @param array $rover
     * @return bool
     */
    public static function canMove(array $plateau, array $rover): bool
    {
        $result = false;
        $px = $plateau['x'];
        $py = $plateau['y'];

        $rx = $rover['x'];
        $ry = $rover['y'];

        $rotate = $rover['rotate'];

        if ($rotate == 'N' && $ry + 1 <= $py) {
            $result = true;
        }

        if ($rotate == 'S' && $ry - 1 >= 0) {
            $result = true;
        }

        if ($rotate == 'W' && $rx - 1 >= 0) {
            $result = true;
        }

        if ($rotate == 'E' && $rx + 1 <= $px) {
            $result = true;
        }

        return $result;
    }

    /**
     * @param array $rover
     * @return array
     */
    public static function move(array $rover): array
    {
        $rx = $rover['x'];
        $ry = $rover['y'];

        $rotate = $rover['rotate'];

        switch ($rotate) {
            case 'N':
                $ry++;
                break;
            case 'S':
                $ry--;
                break;
            case 'W':
                $rx--;
                break;
            case 'E':
                $rx++;
                break;
            default:
                break;
        }
        return [$rx, $ry, $rotate];
    }

}