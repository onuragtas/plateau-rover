<?php

namespace Controllers;

use BaseTest;
use Resources\PlateauResource;

/**
 *
 */
class PlateauControllerTest extends BaseTest
{

    /**
     * @return void
     */
    public function testAddPlateau()
    {
        $resource = new PlateauResource();
        $id = $resource->add(167, 168);
        $result = $resource->get($id);
        $resource->delete($id);
        $this->assertEquals(167, $result['x']);
        $this->assertEquals(168, $result['y']);
    }

    /**
     * @return void
     */
    public function testListPlateau()
    {
        $resource = new PlateauResource();
        $this->assertIsBool(true, $resource->list() !== null);
    }

    /**
     * @return void
     */
    public function testRemovePlateau()
    {
        $resource = new PlateauResource();
        $id = $resource->add(167, 168);
        $result = $resource->delete($id);
        $this->assertIsBool(true, $result);
    }
}
