<?php

namespace Controllers;

use Core\Controller;
use Exceptions\ParameterException;
use Exceptions\PlateauMissingException;
use Exceptions\RoverMissingException;
use Resources\RoverResource;

class RoverController extends Controller
{

    /**
     * @return array
     * @throws ParameterException
     * @throws PlateauMissingException
     */
    public function add(): array
    {
        $parameters = $this->getJsonRequest();
        if (!isset($parameters['plateau_id']) || !isset($parameters['x']) || !isset($parameters['y']) || !isset($parameters['rotate'])) {
            throw new ParameterException('Eksik parametre');
        }

        $resource = new RoverResource();
        $id = $resource->add($parameters['plateau_id'], $parameters['x'], $parameters['y'], $parameters['rotate']);
        return ['id' => $id];
    }

    /**
     * @param int $plateauId
     * @param int $roverId
     * @return array
     * @throws ParameterException
     * @throws RoverMissingException
     */
    public function move(int $plateauId, int $roverId): array
    {
        $parameters = $this->getJsonRequest();

        if (empty($parameters['commands'])) {
            throw new ParameterException('Eksik parametre');
        }

        $commands = $parameters['commands'];
        $resource = new RoverResource();
        $resource->moveRover($plateauId, $roverId, $commands);
        if (empty($resource->getMessages())) {
            return ['moved' => true];
        } else {
            return $resource->getMessages();
        }
    }

    /**
     * @param int $plateauId
     * @return array
     */
    public function list(int $plateauId): array
    {
        $resource = new RoverResource();
        return $resource->list($plateauId);
    }

    /**
     * @param int $pId
     * @param int $id
     * @return array
     */
    public function remove(int $pId, int $id): array
    {
        $resource = new RoverResource();
        return [
            'deleted' => $resource->delete($pId, $id)
        ];
    }

    /**
     * @param int $pId
     * @param int $id
     * @return array
     */
    public function get(int $pId, int $id): array
    {
        $resource = new RoverResource();
        return $resource->get($pId, $id);
    }
}