<?php

function handleThrowable()
{
    header('content-type: application/json');
    echo json_encode([
        'success' => false,
        'message' => 'System Error'
    ]);
}

set_exception_handler('handleThrowable');

// vendor autoload
spl_autoload_register(function (string $className) {
    $classPath = __DIR__ . '/../src/' . str_replace('\\', '/', $className) . '.php';
    $class = '../src/' . str_replace('\\', '/', $className) . '.php';
    if (is_readable($classPath)) {
        require $class;
    }
});

define('PROJECT_DIR', __DIR__ . '/../');

require '../vendor/autoload.php';
require '../routes/route.php';