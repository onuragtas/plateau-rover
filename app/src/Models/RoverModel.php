<?php

namespace Models;

use Core\Model;

/**
 *
 */
class RoverModel extends Model
{

    /**
     *
     */
    public function __construct()
    {
        $this->file = 'data/rover.json';
        parent::__construct();
    }
}