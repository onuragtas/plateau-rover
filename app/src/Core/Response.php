<?php

namespace Core;

use Exception;

/**
 *
 */
class Response
{

    /**
     * @return void
     */
    public function run()
    {
        header('content-type: application/json');

        try {
            echo json_encode([
                'success' => true,
                'data' => Router::getInstance()->runRoute()
            ]);
        } catch (Exception $e) {
            echo json_encode([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}