<?php

namespace Repositories;

use Core\IRepository;
use Core\Repository;
use Models\RoverModel;

/**
 *
 */
class RoverRepository extends Repository implements IRepository
{

    /**
     *
     */
    public function __construct()
    {
        $this->model = new RoverModel();
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->getAll();
    }

    /**
     * @param array $data
     * @return int
     */
    public function add(array $data): int
    {
        return $this->setData($data);
    }

    /**
     * @param int $i
     * @return array
     */
    public function get(int $i): array
    {
        return $this->getItem($i);
    }

    /**
     * @param int $i
     * @return bool
     */
    public function delete(int $i): bool
    {
        return $this->deleteItem($i);
    }
}