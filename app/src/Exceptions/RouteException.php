<?php

namespace Exceptions;

use Exception;

/**
 *
 */
class RouteException extends Exception
{

    /**
     *
     */
    const MISS_ROUTE = 'Missing Route';
}